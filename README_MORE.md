🐳🖧 OpenNetAdmin (ONA) Docker — More Readme
=============================================

### 🔀 HTTP-Proxy

If you plan to proxy your container trough another webserver such at your host, please note that the same base-subdirectory has to be used. This docker image is configured to use `ona/` as subdirectory. To modify this default, edit [`ona/apache2.conf`](https://gitlab.com/raabf/ona-docker/blob/master/ona/apache2.conf) and build the image on your own.

A suitable proxy directive for your host-apache2 instance could look like this:

    <Location /ona>
        RequestHeader unset Accept-Encoding
        ProxyPreserveHost on

        AddOutputFilterByType SUBSTITUTE text/html
        Substitute "s|http://example.org/ona|https://example.org/ona|ni"

        ProxyPassReverse /
    </Location>

    ProxyPass /ona http://127.0.0.1:8666/ona
    ProxyPassReverse /ona http://127.0.0.1:8666/ona
    ProxyRequests Off

It assumes that your host webserver is accessible via `example.org` and that you have the required modules loaded: `mod_proxy`, `mod_filter`, `mod_substitude`.

It is also possible to omit `--publish 0.0.0.0:8666:80` when the container is created and then directly point to the container IP with port 80 (for example `http://172.17.0.1:80` instead of `http://localhost:8666/`). But remember to assign a static IP address to the container.
