🐳🖧 OpenNetAdmin (ONA) Docker
=====================================

[![docker automated](https://img.shields.io/docker/automated/raabf/latex-versions.svg)](https://hub.docker.com/r/raabf/ona)
[![maintained](https://img.shields.io/maintenance/yes/2024.svg)](https://gitlab.com/raabf)
[![licence](https://img.shields.io/github/license/raabf/ona-docker.svg)](https://gitlab.com/raabf/ona-docker/blob/master/LICENSE)

[![🐋Dockerhub](https://img.shields.io/badge/🐋Dockerhub-🖧raabf/ona-yellow.svg "Dockerhub")
![docker stars](https://img.shields.io/docker/stars/raabf/ona.svg)
![docker pulls](https://img.shields.io/docker/pulls/raabf/ona.svg)](https://hub.docker.com/r/raabf/ona)
[![Dockerfile](https://img.shields.io/badge/🗎-Dockerfile-orange.svg)](https://gitlab.com/raabf/ona-docker/blob/master/ona/Dockerfile "Dockerfile")

The image is automatically rebuilt monthly to include changes in [php](https://hub.docker.com/_/php/) and [git-develop](https://github.com/opennetadmin/ona/tree/develop) as well as [git-master](https://github.com/opennetadmin/ona/tree/master) branches.

## 📑️ Table of Contents
<!-- MarkdownTOC -->

- [🐳🖧 OpenNetAdmin (ONA) Docker](#-opennetadmin-ona-docker)
  - [📑️ Table of Contents](#️-table-of-contents)
  - [ℹ️ ONA Description](#ℹ️-ona-description)
  - [🏷 Supported tags](#-supported-tags)
  - [🛠 Installation](#-installation)
    - [📀 Get Image](#-get-image)
      - [⬇️ Pull from Dockerhub](#️-pull-from-dockerhub)
      - [🔨 Manual Building](#-manual-building)
    - [🎽 Create Container](#-create-container)
    - [🚢 Setup ONA](#-setup-ona)
    - [⬆️ Upgrades](#️-upgrades)
    - [🔀 HTTP-Proxy](#-http-proxy)
  - [💡 Contributing](#-contributing)

<!-- /MarkdownTOC -->

## ℹ️ ONA Description

[🌐 http://opennetadmin.com](http://opennetadmin.com)

OpenNetAdmin provides a database managed inventory of your IP network. Each subnet, host, and IP can be tracked via a centralized AJAX enabled web interface that can help reduce tracking errors. A full CLI interface is available as well to use for scripting and bulk work. We hope to provide a useful Network Management application for managing your IP subnets, hosts and much more. Stop using spreadsheets to manage your network! Start doing proper IP address management!

 + Full CLI interface for batch and scripting
 + Plugin system to extend functionality
 + Audit managed subnets and discover new IPs
 + Manage DNS and DHCP server configs, archive host configs
 + [And much more …](http://opennetadmin.com/features.html)

[![Display Host Screenshot](http://opennetadmin.com/images/display_host.png)](http://opennetadmin.com/features.html)

## 🏷 Supported tags

The “🖧ONA” column contains a reference to the git ref from which the respective images were built from. The “🏷tag ONA version” and “🏷tag with 🕸️php” are both
container-tags you can reference – Tags in the same row are pointing to the same image and hence are aliasing each other. Additionally two images are als tagged with
“latest” and “testing”, which can be seen in the “📋Notes” column.

There are two type of images “Static Build” and “Auto Build”.
The first one are images which were built from the referenced ONA version and php-patch version and are static and will not change – Useful if you need reproducibility from the docker registry.
The “Auto Build” is building in regular schedules (by a cron job) and are referencing the php-minor version, hence always take the newest php-patch version – This is useful if you want to always have the newest php version for bug fixing and security reasons.
In case of the `develop` and `master` tags, they take whatever the same-named git branches contain during build.
You can still get reproducability when you store your current local state by `docker image save`.

|                                                             🖧ONA | 🏷tag ONA version           | 🏷tag with 🕸️php       | 📋Notes                                                                                                                                                                                                                                                    |
| --------------------------------------------------------------: | :------------------------ | :------------------ | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [git-develop](https://github.com/opennetadmin/ona/tree/develop) | `develop`                 | `develop-php8.2`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 |                           | `develop-php8.1`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 |                           | `develop-php7.4`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%9A%AB-Auto%20Rebuild-red.svg "Regularily rebuilt") deprecated - no builds anymore                                                                                                              |
|   [git-master](https://github.com/opennetadmin/ona/tree/master) |                           | `master-php8.2`     | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 | `master`                  | `master-php8.1`     | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt") ![Current testing-tag](https://img.shields.io/badge/🏷-testing-yellow.svg "Current testing-tag")                                              |
|                                                                 |                           | `master-php7.4`     | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%9A%AB-Auto%20Rebuild-red.svg "Regularily rebuilt") deprecated - no builds anymore                                                                                                              |
|     [v19.0.2](https://github.com/opennetadmin/ona/tree/v19.0.2) |                           | `v19.0.2-php8.3`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 | `v19.0.2`, `v19.0`, `v19` | `v19.0.2-php8.3.7`  | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")    ![Current latest-tag](https://img.shields.io/badge/🏷-latest-yellow.svg "Current latest-tag")                                                            |
|                                                                 |                           | `v19.0.2-php8.3.6`  | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")                                                                                                                                                           |
|                                                                 |                           | `v19.0.2-php8.2`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 |                           | `v19.0.2-php8.2.19` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")                                                                                                                                                           |
|                                                                 |                           | `v19.0.2-php8.2.18` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")                                                                                                                                                           |
|                                                                 |                           | `v19.0.2-php7.4.33` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once") with-working-ldap¹                                                                                                                                        |
|     [v19.0.1](https://github.com/opennetadmin/ona/tree/v19.0.1) |                           | `v19.0.1-php8.3`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 |                           | `v19.0.1-php8.3.7`  | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")                                                                                                                                                           |
|                                                                 |                           | `v19.0.1-php8.2`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 |                           | `v19.0.1-php8.2.19` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")                                                                                                                                                           |
|                                                                 |                           | `v19.0.1-php8.1`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 | `v19.0.1`                 | `v19.0.1-php8.1.28` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")                                                                                                                                                           |
|                                                                 |                           | `v19.0.1-php7.4`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%9A%AB-Auto%20Rebuild-red.svg "Regularily rebuilt")  with-working-ldap¹, deprecated - no builds anymore                                                                                         |
|                                                                 |                           | `v19.0.1-php7.4.33` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once") with-working-ldap¹                                                                                                                                        |
|     [v19.0.0](https://github.com/opennetadmin/ona/tree/v18.1.1) |                           | `v19.0.0-php8.2`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt") On upgrade to v19 [it is more recommended than normally](https://github.com/opennetadmin/ona/releases/tag/v19.0.0) to make a backup before. |
|                                                                 |                           | `v19.0.0-php8.2.19` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")                                                                                                                                                           |
|                                                                 |                           | `v19.0.0-php8.1`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%94%84-Auto%20Rebuild-red.svg "Regularily rebuilt")                                                                                                                                             |
|                                                                 | `v19.0.0`                 | `v19.0.0-php8.1.28` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once")                                                                                                                                                           |
|                                                                 |                           | `v19.0.0-php7.4`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%9A%AB-Auto%20Rebuild-red.svg "Regularily rebuilt")  with-working-ldap¹, deprecated - no builds anymore                                                                                         |
|                                                                 |                           | `v19.0.0-php7.4.33` | ![Built once](https://img.shields.io/badge/%F0%9F%AA%A8-Static%20build-green.svg "Built once") with-working-ldap¹                                                                                                                                        |
|     [v18.1.1](https://github.com/opennetadmin/ona/tree/v18.1.1) | `v18.1.1`                 | `v18.1.1-php5`      | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%9A%AB-Auto%20Rebuild-red.svg "Regularily rebuilt") deprecated - no builds anymore                                                                                                              |
|                                                                 |                           | `v18.1.1-php7.0`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%9A%AB-Auto%20Rebuild-red.svg "Regularily rebuilt") deprecated - no builds anymore                                                                                                              |
| [v17.12.22](https://github.com/opennetadmin/ona/tree/v17.12.22) | `v17.12.22`               | `v17.12.22-php5`    | ![Regularily rebuilt](https://img.shields.io/badge/%F0%9F%9A%AB-Auto%20Rebuild-red.svg "Regularily rebuilt") deprecated - no builds anymore                                                                                                              |

with-working-ldap¹: There is a build with php7, because the ldap-authentication module from ONA does not work yet with php8. Athough that means that ONA is not fully compatible with php8 yet, I still use php8 as default because (1) it seems it only affects the ldap-authentication, which many probably do not need, and (2) php7 has several security vulnerabilites which won’t be fixed as php7 as EOL.

## 🛠 Installation

### 📀 Get Image

Pull the image or build it manually.

#### ⬇️ Pull from Dockerhub

Pull from Dockerhub. The tag `latest` can be replaced by any of the „Supported tags“ section:

    docker pull raabf/ona:latest

#### 🔨 Manual Building

Checkout or download the [repository](https://gitlab.com/raabf/ona-docker/) and run:

    ENGINE=docker ONA_VERSION="v19.0.1" PHP_VERSION="8.2" IMAGE_NAME="ona" IMAGE_TAG="mytag" DOCKERFILE_PATH='ona/Dockerfile' ona/scripts/build.sh

<!-- docker build --tag=myona --build-arg PHP_VERSION=‘8.1’ --build-arg ONA_VERSION=“v19.0.1” . -->

 + `PHP_VERSION='8.1'` (optional) can be any version available at [php](https://hub.docker.com/_/php/) (all `*-apache` tags are valid; examples: `7`, `5.6`, `7.1.22`, `7.3.0RC1`, `8`, `8.2`)
 + `ONA_VERSION="v19.0.2"` (required) can be any branch, git-tag, or commit-hash from [ONA](https://github.com/opennetadmin)
 + `ENGINE=docker` (optional) which cli command to use. Can be a full path, for example `/usr/bin/docker`, or something in $PATH, for example `podman`. Defaults to `docker`.
 + `IMAGE_NAME=ona` (optional) The name of the image. Defaults to `ona`.
 + `IMAGE_TAG="mytag"` (required) The tag of the image.
 + `DOCKERFILE_PATH=ona/Dockerfile` (optional) Path to the Dockerfile to build. Defaults to `ona/Dockerfile`.

### 🎽 Create Container

Execute the following

    docker run --detach --name ona --publish 0.0.0.0:8666:80 -v /etc/ona/local/:/opt/ona/www/local -v /etc/ona/etc/:/opt/ona/etc -v /etc/timezone:/etc/timezone:ro -v /etc/localtime:/etc/localtime:ro raabf/ona:latest

 + `--publish 0.0.0.0:8666:80` ONA is reachable at the interface `0.0.0.0` (=any) and the port `8666`. Adapt to your needs.
 + `-v /etc/ona/local/:/opt/ona/www/local` `/etc/ona/local/` is the configuration directory of your ONA instance and the path must already exist. It must be a permanent storage. If omitted docker will create an unnamed volume (use `docker inspect` to find out this directory).
 + `-v /etc/ona/etc/:/opt/ona/etc` Specifies another permanent configuration directory with the same properties. However, it might keep empty if you do not install any plugins, or do not use their client scripts on this host.
 + `-v /etc/timezone:/etc/timezone:ro` `-v /etc/localtime:/etc/localtime:ro` timezone information required by PHP.
 + `raabf/ona:latest` The image pulled earlier.

As an alternative, you can use a docker-compose file (I inlcuded an example with a suitable MariaDB image).
Put the following in an own folder with a specific filename, namely `./ona/docker-compose.yml`:

```yaml
---
services:
  ona:
    image: raabf/ona:latest
    container_name: ona
    # This containes does not dirctly get the database credentials, please
    # set them during ONA Installation (continue reading in the README.md)
    volumes:
      - /etc/ona/local/:/opt/ona/www/local
      - /etc/ona/etc/:/opt/ona/etc
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - 8666:80
    restart: unless-stopped

  # Put “sql” in the “Database Host” field during the ONA Installation
  sql:
    image: mariadb:10.4
    container_name: ona-sql
    environment:
      # Put this in the “Database Admin Password” field during the ONA Installation
      - MYSQL_ROOT_PASSWORD=YourRootSecretPassword

      # The following would be common options for a MariaDB container initialization,
      # however, they are set during the ONA Installation, so do not provide them here.
      # Anyway this would be the content of the “Database Name” filed (without the `ona_` prefix
      # as it is automatically added by installer) during the ONA Installation
      # - MYSQL_DATABASE=ona_default
      # This would be the “Application Database User Name” field during the ONA Installation
      # - MYSQL_USER=ona_sys
      # This would be the “Application Database User Password” field during the ONA Installation
      # - MYSQL_PASSWORD=YourOnaSysSecretPassword
    volumes:
      - db:/var/lib/mysql
    restart: unless-stopped

volumes:
  db:

```

Then execute `docker compose up -d` in the `ona` folder.


### 🚢 Setup ONA

Then you have to find out the container name and copy it:

    docker ps

We assume in the following that your container name is `ona` (If you choose the docker-compose from above, `ona` is the correct container name).

If you are running a *rootless* (i.e. skip this step if you are using `docker`) container engine, you have to set permissions on the volume:

    podman unshare chown www-data:www-data -R /etc/ona/local/
    podman unshare chown www-data:www-data -R /etc/ona/etc/

Moreover, we have to initialize the config directory and timezone of your server. This has to be done only once after creating a new container. Execute the following (existing files in the config directory will not be overwritten):

    docker exec --interactive --tty ona ./init_conf.sh

Now restart your container to apply changes:

    docker restart ona

ONA is now reachable at [http://localhost:8666/ona](http://localhost:8666/ona) and present you the ONA Web Installation.

OR, you might also go through the ONA CLI Installation on the command line (It does the same checks and presents the same fields as the Web Installation):

    docker exec --interactive --tty ona php /opt/ona/install/installcli.php

Note that this image is shipped without a database. Use either an MySQL/MariaDB instance running at your one of your hosts or use an [MariaDB Docker](https://hub.docker.com/_/mariadb/) image.

Now follow the [ONA Installation guide](https://github.com/opennetadmin/ona/wiki/Install#web-install-method) to complete your installation.

❗ATTENTION: *After* Installation you have to change the host part in the MySQL user table of the “Application Database User” (by default “ona_sys”) you provide during the ONA Installation.
ONA will create the MySQL/MariaDB user with a host, which limits connections in a way so that ONA cannot connect to the database¹.

To fix that run the following command. “ona-sql” is the name of your MySQL/MariaDB container, and you will be asked for its `MYSQL_ROOT_PASSWORD` (You have set this during docker run of the MySQL/MariaDB container). 
The `"sql"` is the service-name (i.e. the hostname, which can be different to the container name) of your MySQL/MariaDB container.:
If you have kept the default “ona_sys” during ONA Installation used the docker-compose file from above, you can use the following command as it is:

    docker exec -it ona-sql mysql --user root --password -e 'RENAME USER "ona_sys"@"sql" TO "ona_sys"@"%";'

Then restart the continer:

    docker restart ona-sql

---
¹ If you are interested why: In docker-networks you usually just use the service-name to reference other containers, which is technically the hostname (i.e. without full FQDN) of the container.
MySQL/MariaDB containers ships by default with `skip-name-resolve=true`, which means that IP-Addresses are *not* resolved to its domain - as
the ONA container will connecty by its IP-Address, it can never match  to the host in the MySQL/MariaDB user table. Even if you set `skip-name-resolve=false`, it will not match as the
IP Address will resolve to the FQDN (which includes the docker-network name, so for the above docker-compose file `ona.ona-generic_default`) and hence will never match to the hostname.

### ⬆️ Upgrades

If upgrade to a container with a never ONA version, but with an existing database you might have to do a manual database migration by calling [http://localhost:8666/ona/?runinstaller=y](http://localhost:8666/ona/?runinstaller=y), see also the [Upgrades Wiki](https://github.com/opennetadmin/ona/wiki/Upgrades).

### 🔀 HTTP-Proxy

Due to docker-hub’s maximum character limit, you find the rest at [README_MORE](https://gitlab.com/raabf/ona-docker/-/blob/master/README_MORE.md).

## 💡 Contributing

If something is missing or not working, then I’m happy for any contribution. You can find the [repository](https://gitlab.com/raabf/ona-docker/) and [issues board](https://gitlab.com/raabf/ona-docker/issues) at GitLab.
