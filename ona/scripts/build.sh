#!/bin/bash

ENGINE="${ENGINE:-docker}"
DOCKERFILE_PATH="${DOCKERFILE_PATH:-ona/Dockerfile}"

echo "--build.sh called--"
echo "build.sh: ENGINE '$ENGINE'"
echo "build.sh: IMAGE_NAME '$IMAGE_NAME'"
echo "build.sh: DOCKERFILE_PATH '$DOCKERFILE_PATH'"
echo "build.sh: CACHE_TAG '$CACHE_TAG'"
echo "build.sh: IMAGE_TAG '$IMAGE_TAG'"
echo "build.sh: DOCKER_REGISTRY_DOMAIN '$DOCKER_REGISTRY_DOMAIN'"
echo "build.sh: DOCKER_REGISTRY_REPO '$DOCKER_REGISTRY_REPO'"
echo "build.sh: SOURCE_BRANCH '$SOURCE_BRANCH'"
echo "build.sh: SOURCE_COMMIT '$SOURCE_COMMIT'"
echo "build.sh: PUSH_ENABLED '$PUSH_ENABLED'"

# extracts the PHP version and the ONA version from the docker-tag
# the format must be `[ONA]-php[PHP]` or `[ONA]`
#  + master-php7
#  + v18.1.1-php7.2
#  + v18.1.1
PHP_VERSION="${PHP_VERSION:-$(echo "$IMAGE_TAG" | sed 's,[^-]*-php\([^\-]*\).*,\1,')}"
echo "build hook: PHP_VERSION '$PHP_VERSION'"

ONA_VERSION="${ONA_VERSION:-$(echo "$IMAGE_TAG" | sed 's,\([^-]*\)-.*,\1,')}"
echo "build hook: ONA_VERSION '$ONA_VERSION'"

DOCKER_REGISTRY_IMAGE_NAME="${DOCKER_REGISTRY_REPO:+${DOCKER_REGISTRY_DOMAIN:-docker.io}/$DOCKER_REGISTRY_REPO}"
FINAL_IMAGE_NAME=${IMAGE_NAME:-${DOCKER_REGISTRY_IMAGE_NAME:-ona}}
echo "build hook: FINAL_IMAGE_NAME '$FINAL_IMAGE_NAME'"


build_args=''

# This will fetch the commit hash of ONA_VERSION. If ONA_VERSION is already a
# commit hash, it will fetch the full length. Note that there are multiple "sha"
# fileds in the json return, and it is assumed that the commit sha is the first
# result. xargs will trim whitespaces.
APP_VCS_REF="$(curl --silent "https://api.github.com/repos/opennetadmin/ona/commits/${ONA_VERSION}" | sed -En 's/"sha": "(.*)",/\1/p' | head --lines=1 | xargs)"

if [ -n "$PHP_VERSION" ]; then
    build_args=(${build_args} --build-arg PHP_VERSION="$PHP_VERSION")
fi

$ENGINE build --build-arg BUILD_DATE="$(date -u +"%Y-%m-%dT%H:%M:%SZ")" \
             --build-arg VCS_REF="$(git rev-parse --short HEAD)" \
             ${build_args[@]} \
             --build-arg ONA_VERSION="$ONA_VERSION" \
             --build-arg APP_VCS_REF="$APP_VCS_REF" \
             --file "$DOCKERFILE_PATH" --tag "$FINAL_IMAGE_NAME:$IMAGE_TAG" . || exit $?

if [[ "$PUSH_ENABLED" == 'true' ]]; then
    $ENGINE push "$FINAL_IMAGE_NAME:$IMAGE_TAG" || exit $?
fi