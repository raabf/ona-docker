#!/bin/bash

ENGINE="${ENGINE:-docker}"

echo "--tag_alias.sh script called--"
echo "tag_alias.sh: IMAGE_NAME '$IMAGE_NAME'"
echo "tag_alias.sh: ENGINE '$ENGINE'"
echo "tag_alias.sh: DOCKERFILE_PATH '$DOCKERFILE_PATH'"
echo "tag_alias.sh: CACHE_TAG '$CACHE_TAG'"
echo "tag_alias.sh: IMAGE_TAG '$IMAGE_TAG'"
echo "tag_alias.sh: DOCKER_REGISTRY_DOMAIN '$DOCKER_REGISTRY_DOMAIN'"
echo "tag_alias.sh: DOCKER_REGISTRY_REPO '$DOCKER_REGISTRY_REPO'"
echo "tag_alias.sh: SOURCE_BRANCH '$SOURCE_BRANCH'"
echo "tag_alias.sh: SOURCE_COMMIT '$SOURCE_COMMIT'"
echo "tag_alias.sh: PUSH_ENABLED '$PUSH_ENABLED'"

declare -A aliasmap

# I use php8.1 for `latest` (i.e. as the default php version), because it is also used
# in https://github.com/opennetadmin/ona/ Dockerfile & Vagrantfile as the default
# php version and hence likely the best tested one.
# The current ubuntu latest is “noble” which ships with php8.3, so for ONA v19.0.2 it is tested with this.
aliasmap["v19.0.0-php8.1.28"]="v19.0.0"
aliasmap["v19.0.1-php8.1.28"]="v19.0.1"
aliasmap["v19.0.2-php8.3.7"]="v19.0.2 v19.0 v19 latest"
aliasmap["master-php8.1"]="master testing"
aliasmap["develop-php8.2"]="develop"

mapfile -t array < <(echo "${aliasmap[${IMAGE_TAG}]}")
echo "tag_alias.sh: use alias list '${array[@]}'"

DOCKER_REGISTRY_IMAGE_NAME="${DOCKER_REGISTRY_REPO:+${DOCKER_REGISTRY_DOMAIN:-docker.io}/$DOCKER_REGISTRY_REPO}"
FINAL_IMAGE_NAME=${IMAGE_NAME:-${DOCKER_REGISTRY_IMAGE_NAME:-ona}}
echo "tag_alias.sh: FINAL_IMAGE_NAME '$FINAL_IMAGE_NAME'"

for aliastag in ${array[@]}; do

	if [[ ${IMAGE_TAG%%-*} == "arm"* ]]; then
		# Prefix the alias tag with the architecture if not x86 (e.g armhf-texlive2017)
		aliastag="${IMAGE_TAG%%-*}-${aliastag}"
	fi


    echo "tag_alias.sh: tag '$FINAL_IMAGE_NAME:$aliastag'"
    docker tag "$FINAL_IMAGE_NAME:$IMAGE_TAG" "$FINAL_IMAGE_NAME:$aliastag" || exit $?

	if [[ "$PUSH_ENABLED" == 'true' ]]; then
		echo "tag_alias.sh: push '$FINAL_IMAGE_NAME:$aliastag'"
	    docker push $FINAL_IMAGE_NAME:$aliastag || exit $?
	fi

done

